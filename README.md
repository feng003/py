#py
backup  备份目录
conf    配置目录
dist    打包目录
www		web目录
	--static 静态文件
	--templates 模板文件


python 3.4.x
pip3 install aiohttp  (异步框架)
pip3 install jinjia2  (前端模板引擎)
pip3 install aiomysql (mysqld python异步驱动程序)