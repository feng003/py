# -*- coding: utf-8 -*-
# 来判断对象类型，使用type()函数：
res = type(123)
print(res)

fn = type(abs)
print(fn)

result = type(None)
print(result)

# 判断class的类型，可以使用isinstance()函数。


# 获得一个对象的所有属性和方法，可以使用dir()函数，它返回一个包含字符串的list，比如，获得一个str对象的所有属性和方法：
rs = dir('ABC')
print(rs)
# length = len('ABC')
length = 'ABC'.__len__()
print(length)

class MyDog(object):
	def __len__(self):
		return 10

dog = MyDog()
res = len(dog)
print(res)

# 配合getattr()、setattr()以及hasattr()，我们可以直接操作一个对象的状态

def readImage(fp):
	if hasattr(fp,'read'):
		return readData(fp)
	return None

fp = './oop_8.py';
res = readImage(fp)
print(res)

