# -*- coding: utf-8 -*-
# 切片（Slice）操作符   对字符串切片
L = list(range(100))
print(L[0:9]) 
print(L[-10:]) 
# 通过for循环来遍历这个list或tuple，这种遍历我们称为迭代（Iteration）。
# Python内置的enumerate函数可以把一个list变成索引-元素对,这样就可以在for循环中同时迭代索引和元素本身
from collections import Iterable
print(isinstance('abc',Iterable))
print(isinstance(123,Iterable))
for i,value in enumerate(['a','b','c']):
	print(i,value)
# 任何可迭代对象都可以作用于for循环，包括我们自定义的数据类型，只要符合迭代条件，就可以使用for循环。

import os
# 列表生成式
print([d for d in os.listdir('.')])

# 生成器 generator
g = (d for d in os.listdir('.'))
# print(next(g))
for n in g:
	print(n)
# 著名的斐波拉契数列（Fibonacci），除第一个和第二个数外，任意一个数都可由前两个数相加得到
def fib(max):
	n,a,b = 0,0,1
	while n<max:
		print(b)
		a,b = b,a+b
		n = n+1
	return print("done")
fib(10)

# generator的工作原理，它是在for循环的过程中不断计算出下一个元素，并在适当的条件结束for循环。对于函数改成的generator来说，遇到return语句或者执行到函数体最后一行语句，就是结束generator的指令，for循环随之结束。

# 普通函数调用直接返回结果;generator函数的“调用”实际返回一个generator对象。

# 可以直接作用于for循环的数据类型(可迭代对象 Iterable   通过isinstance()判断一个对象是否是Iterable对象)有以下几种：
# 一类是集合数据类型，如list、tuple、dict、set、str等；
# 一类是generator，包括生成器和带yield的generator function。

# 凡是可作用于for循环的对象都是Iterable类型；
# 凡是可作用于next()函数的对象都是Iterator类型，它们表示一个惰性计算的序列；
# 集合数据类型如list、dict、str等是Iterable但不是Iterator，不过可以通过iter()函数获得一个Iterator对象。
# Python的for循环本质上就是通过不断调用next()函数实现的.
