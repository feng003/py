# -*- coding: utf-8 -*-
# 多进程  fork()系统调用(linux特有)
# 。普通的函数调用，调用一次，返回一次，但是fork()调用一次，返回两次，因为操作系统自动把当前进程（称为父进程）复制了一份（称为子进程），然后，分别在父进程和子进程内返回。

# import os
# print("Process (%s) start..." % os.getpid())
# pid = os.fork()
# if pid==0:
# 	print("I am process (%s) and my parent is %s" % (os.getpid(),os.getpid()))
# else:
# 	print('I (%s) just created a child process (%s)' % (os.getpid(),pid))


from multiprocessing import Process
import os
def run_proc(name):
	print("Run child process(%s).."%(name,os.getpid()))

if __name__=='__mian__':
	print('Parent process %s.'% os.getpid())
	p=Process(target=run_proc,args=('test',))
	print('child process will start .')
	p.start()
	p.join()
	print("child process end")