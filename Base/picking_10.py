# -*- coding: utf-8 -*-
# picking 把变量从内存中变成可存储或传输的过程称之为序列化(serialization，marshalling，flattening)
# unpicking
import pickle
d = dict(name='Bob',age=20,score=80)
# res = pickle.dumps(d)
# print(res)
# pickle.dumps()方法把任意对象序列化成一个bytes，或用pickle.dump()直接把对象序列化写入一个file-like ojbect
# f = open('./one.txt','wb')
# pickle.dump(d,f)
# f.close()

# pickle.loads()反序列化
f = open('./one.txt','rb')
d = pickle.load(f)
f.close
print(d)
# json    {}---[]---"string"---1234---true/false---null
# python  dict--list--str--int/float--True/False--None

import json
d = dict(name="Bob",age=20,score=80)
res = json.dumps(d)
print(res)

class Student(object):
	def __init__(self,name,age,score):
		self.name = name
		self.age = age
		self.score = score

def student2dict(std):
	return{
		"name":std.name,
		"age":std.age,
		"score":std.score
	}
def dict2student(d):
	return Student(d['name'],d['age'],d['score'])

s = Student('Bob',20,80)
print(json.dumps(s,default=student2dict))

