import random
def weixin_divide_hongbao(money, n):
    divide_table = [random.randint(1, 10000) for x in range(0, n)]
    sum_ = sum(divide_table)
    return [x*money/sum_ for x in divide_table]

res = weixin_divide_hongbao(10,5)
print(res)