# -*- coding: utf-8 -*-
# 多重继承   主线都是单一继承下来的

class Runabele(object):
	def run(self):
		print('Run...')
class Flyable(object):
	def fly(self):
		print('Fly...')

class Dog(Mammal,Runabele):
	pass

class Bat(Mammal,Flyable):
	pass

# MixIn 同时继承多个class
class Cat(Mammal,RunabeleMixIn,FlyableMixIn):
	pass

# python自带的TCPServer UDPServer网络服务，要同时服务多个用户就必须使用多进程或多线程模型
class MyTCPServer(TCPServer,ForkingMixIn):
	pass

class MyUDPServer(UDPServer,ThreadingMixIn)
	pass

# 由于Python允许使用多重继承，因此，MixIn就是一种常见的设计。
# 只允许单一继承的语言（如Java）不能使用MixIn的设计