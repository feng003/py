#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# list []  tuple () 
#list [] list是一种有序的集合，可以随时添加和删除其中的元素。
arr = ['python','list','len()','array','tuple']
print(arr[-1])
#append 元素末尾追加
arr.append('Append')
print(arr)
#insert 指定位置添加
arr.insert(2,'insert')
print(arr)
#pop 删除指定元素 pop(i)

L = [['Apple','Google','Ms'],['Java','Python','php'],['linux','mysql']]
print(L[0][0])

#tuple () 有序列表叫元组。tuple和list非常类似，但是tuple一旦初始化就不能修改。
#tuple所谓的“不变”是说，tuple的每个元素，指向永远不变。即指向'a'，就不能改成指向'b'，指向一个list，就不能改成指向其他对象，但指向的这个list本身是可变的！
t = ('a','b',['x','y'])
print(t)
t[2][0] = "xyz"
t[2][1] = "XYZ"
print(t)

# 条件判断 用  str input()读取用户的输入，这样可以自己输入。
#weight = 18
w = input('weight:')
weight = int(w)
if weight<18:
	print('过轻')
elif weight>=18 and weight<25:
	print('正常')
elif weight>=25 and weight<32:
	print('guo zhong ')
else:
	print('fat')