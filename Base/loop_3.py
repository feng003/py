# -*- coding: utf-8 -*-
# dict{} set() range() 

# for x in xrange(1,10):
# res = list(range(101))
# print(res)
sum = 0
for x in range(101):
	sum = sum + x
print(sum)

L = ['bart','List','Adam']
#字符串拼接   + ，join
for i in L:
	print("hello,",i)
# print(i.join("hello ,"))

# dict(map json) 使用键-值（key-value）存储，具有极快的查找速度。dict是用空间来换取时间的一种方法。

d = {'M':80,'G':100,'L':60}
print(d['M'])
# dict的key必须是不可变对象。在Python中，字符串、整数等都是不可变的，因此，可以放心地作为key。而list是可变的，就不能作为key。
# 避免key不存在，1、 in 'M' in d 判断key 2、get方法 d.get['M']
# 删除key  pop(key)

# set()  是一组key的集合，但不存储value。由于key不能重复，所以，在set中，没有重复的key。
s = set([1,2,3,1,4,5])
print(s)
# add(key) 添加元素
# remove(key) 删除元素
# set可以看成数学意义上的无序和无重复元素的集合，两个set可以做数学意义上的交集、并集等操作。
set1 = set(['python','list','len()','array','tuple'])
print(set1)

set2 = set(('a','b',['x','y']))
print(set2)