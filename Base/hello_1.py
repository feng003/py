#!/usr/bin/env python3
# -*- coding: utf-8 -*-
print("hello, python")
#Python使用缩进来组织代码块，请务必遵守约定俗成的习惯，坚持使用4个空格的缩进。
a=100
if a>=0:
	print(a)
else:
	print(-a)

#数据类型 整数  浮点数  字符串( \ 转义) 布尔(Trur False)  空值(None)
print('I\'m \"ok\"!')
# r'' 表示 ''内部的字符串默认不转义   '''...'''的格式表示多行内容
print(r'\\\t\\')

#变量 = 赋值   常量   运算  /   //(结果为整数)
A='abc'
print(A)
s1 = 'Hello , \'Adam\''
print(s1)
s3 = r'hello,"python"!'
print(s3)
s4 = r'''hello,
Lisa!'''
print(s4)
#Python提供了ord()函数获取字符的整数表示，chr()函数把编码转换为对应的字符, 计算str包含多少个字符，可以用len()函数
str1 = b'\xe4\xb8\xad\xe6\x96\x87'.decode('utf-8')
print(str1)
# % 运算符   格式化字符串  %s表示用字符串替换，%d表示用整数替换，有几个%?占位符，后面就跟几个变量或者值，顺序要对应好
str2 = '%2d - %02d' %(45,23)
print(str2)
str3 = '%.3f' % 3.1415926
print(str3)