# -*- coding: utf-8 -*-
# function abs() max() min() int() hex()一个整数转换成十六进制表示的字符串 math.sqrt(x) 数字x的平方根。
# 参数定义的顺序必须是：必选参数、默认参数、可变参数/命名关键字参数和关键字参数。
res = abs(-20)
print(res)

n1 = 255
n2 = 1000
print(hex(n1))
print(hex(n2))

def my_func(x):
	# 对参数做数据类型检查
	if not isinstance(x,(int,float)):
		raise TypeError('bad operand type')
	if x >= 0:
		return x
	else:
		return -x
res = my_func(12.05)
print(res)
# Python的函数返回多值其实就是返回一个tuple
import math
def move(x,y,step,angle=0):
	"""
		移动 move
	"""
	nx = x+step*math.cos(angle)
	ny = y+step*math.sin(angle)
	return nx,ny
r = move(10,50,60,math.pi/6)
print(r)
# ax*x+bx+c=0
def quadraric(a,b,c):
	str = b*b-4*a*c
	if a == 0:
		x = -(c/b)
		return x
	elif int(str) >= 0:
		str1 = (-b + math.sqrt(str))/(2*a)
		str2 = (-b - math.sqrt(str))/(2*a)
		return str1,str2
	else:
		return False
res = quadraric(5,6,1);
print(res)
# x的n次方  默认参数必须指向不变对象！
def power(x,n=3):
	s = 1
	while n>0:
		n = n-1
		s = s*x
	return s
res = power(3)
print(res)
#参数  传入一个list  不变对象   str  None
def add_end(L=None):
	if L is None:
		L=[]
	L.append('END')
	return L
res = add_end([123])
print(res)
# 可变参数 (a*a+b*b+c*c ...) 在参数前面加了一个*号 *nums表示把nums这个list的所有元素作为可变参数传进去
def calc(*number):
	sum=0
	for n in number:
		sum = sum+n*n
	return sum
# res = calc([1,5,3,5])
# print(res)
nums = [1,2,5,3]
print(calc(*nums))
# 关键词参数
