# -*- coding: utf-8 -*-
# read
try:
	f = open('./one.txt','r',encoding='gbk',errors='ignore')
	res = f.read()
	print(res)
finally:
	if f:
		f.close()

# 如果文件很小，read()一次性读取最方便；如果不能确定文件大小，反复调用read(size)比较保险；如果是配置文件，调用readlines()最方便：
f = open('./one.txt','r',encoding='gbk',errors='ignore')
for line in f.readlines():
	print(line.strip())

# write
f = open('./one.txt','w')
f.write('Hello python write file')
f.close()

with open('./one.txt','w') as f:
	f.write('hello python with')

# StringIO和BytesIO是在内存中操作str和bytes的方法，使得和读写文件具有一致的接口。
# 操作 str
from io import StringIO
f = StringIO()
res = f.write('hello')
print(res)
# getvalue()方法用于获得写入后的str
print(f.getvalue())

# 操作 二进制文件
from io import BytesIO
f= BytesIO()
res = f.write('中文'.encode('utf-8'))
print(res)
print(f.getvalue())

# directory dir cd 
import os
print(os.name)
# print(os.uname())   win 系统不提供uname
print(os.environ)
print(os.path.abspath('./'))   #E:\wamp\www\py
dir = os.path.join('./','test')
print(dir)
# os.mkdir(dir) 创建文件夹
# os.rmdir(dir) 删除文件夹


