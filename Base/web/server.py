# server.py

from wsgiref.simple_server import make_server

from hello import application

# 创建一个服务器
httpd = make_server('',8000,application)
print('Serving HTTP on port 8000...')

# 监听http请求
httpd.serve_forever()