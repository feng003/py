# asyncio.py
# 消息循环。我们从asyncio模块中直接获取一个EventLoop的引用，然后把需要执行的协程扔到EventLoop中执行，就实现了异步IO。
# import asyncio

# @asyncio.coroutine
# def hello():
# 	print('Hello world!')

# 	r = yield from asyncio.sleep(5)
# 	print('hello again')

# loop = asyncio.get_event_loop()
# loop.run_until_complete(hello())
# loop.close()

import threading
import asyncio

@asyncio.coroutine
def hellos():
	print('Hello world! (%s)' % threading.currentThread())
	yield from asyncio.sleep(3)
	print('Hello again! (%s)' % threading.currentThread())

loop = asyncio.get_event_loop()

tasks = [hellos(), hellos()]

loop.run_until_complete(asyncio.wait(tasks))
loop.close()