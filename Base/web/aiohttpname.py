# aiohttpname.py

import asyncio 
from aiohttp import web

def index(request):
	return web.Response(body=b'<h2>Index</h2>')

def hello(request):
	yield from asyncio.sleep(1)
	text = '<h1>hello, %s</h1>' % request.match_info['name']
	return web.Response(body=text.encode('utf-8'))

@asyncio.coroutine

def init(loop):
	app = web.Application(loop=loop)
	app.router.add_route('GET','/',index)
	app.router.add_route('GET','/hello/{name}',hello)
	srv = yield from loop.create_server(app.make_handler(),'127.0.0.1',8080)
	print('Server started at http://127.0.0.1:8000...')
	return srv

loop = asyncio.get_event_loop()
loop.run_until_complete(init(loop))
loop.run_forever()

# aiohttp的初始化函数init()也是一个coroutine，loop.create_server()则利用asyncio创建TCP服务。

