# -*- coding: utf-8 -*-
# class instance(object)  类是创建实例的模板，而实例则是一个一个具体的对象。
class Person(object):

	height = '170'	
	def __init__(self,name,score):
		self.__name = name
		self.__score = score

	def get_name(self):
		return self.__name

	def print_score(self):
		print('%s: %s' % (self.__name,self.__score))

class Student(Person):
	# def __init__(self,name,score):
		# self.name  = name
		# self.score = score
	# def print_score(self):
	# 	print('%s: %s' % (self.name,self.score))
	def __init__(self,name,score):
		super(Student,self).__init__(name,score)
		
	def get_grade(self):
		if self.get_score() >= 90:
			return 'A'
		elif self.get_score() >= 60:
			return 'B'
		else:
			return 'C'

bart = Student('bart',100)
# bart.print_score()
# print(bart.get_score())
# print(bart.get_grade())
print(bart.height)
bart.height = '180'
print(bart.height);
print(Person.height)
del bart.height
print(bart.height)
# 不要把实例属性和类属性使用相同的名字，因为相同名称的实例属性将屏蔽掉类属性


# 数据封装
# 如果要让内部属性不被外部访问，可以把属性的名称前加上两个下划线__
# 实例的变量名如果以__开头，就变成了一个私有变量（private），只有内部可以访问，外部不能访问。