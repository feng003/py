# -*- coding: utf-8 -*-
# 摘要算法又称哈希算法、散列算法。它通过一个函数，把任意长度的数据转换为一个长度固定的数据串（通常用16进制的字符串表示）。

import hashlib
# md5 SHA1
md5 = hashlib.md5()
# md5.update('how to use md5 in python hashlib?'.encode('utf-8'))
md5.update('how to use md5 in '.encode('utf-8'))
md5.update('python hashlib?'.encode('utf-8'))
print(md5.hexdigest())

# 任何摘要算法都是把无限多的数据集合映射到一个有限的集合中。这种情况称为碰撞

# 加盐   把登录名作为Salt的一部分来计算MD5，从而实现相同口令的用户也存储不同的MD5。
def cals_md5(password):
	return get_md5(password + 'the-Salt')