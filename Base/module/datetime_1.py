# -*- coding: utf-8 -*-
#!/usr/bin/python 
from datetime import datetime
now = datetime.now()
print(now)
print(type(now))

dt = datetime(2015,5,20,11,30)
print(dt)

# datetime to timestamp
res =dt .timestamp()
print(res)

# timestamp to datetime
t = 1432092600.0
print(datetime.fromtimestamp(t))
print(datetime.utcfromtimestamp(t))

# str to datetime   datetime.strptime()
cday = datetime.strptime('2015-05-18 15:20','%Y-%m-%d %H:%M')
print(cday)

# datetime to str strftime()
print(now.strftime('%a,%b %d %H:%M'))

# datetime +(-) improt timedelta
from datetime import datetime,timedelta
print(now+timedelta(days=1,hours=5))

# local time to utc time  utcnow() 当前的utc时间



