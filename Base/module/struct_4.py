# -*- coding: utf-8 -*-
# struct 解决bytes和其他二进制数据类型的转换。

import struct

# pack 第一个参数是处理指令  >表示字节顺序是big-endian，也就是网络序，I表示4字节无符号整数。
print(struct.pack('>I',10240099))
print(struct.unpack('>IH',b'\xf0\xf0\xf0\xf0\x80\x80'))

s = b'\x42\x4d\x38\x8c\x0a\x00\x00\x00\x00\x00\x36\x00\x00\x00\x28\x00\x00\x00\x80\x02\x00\x00\x68\x01\x00\x00\x01\x00\x18\x00'

print(struct.unpack('<ccIIIIIIHH',s))

# b'B'、b'M'说明是Windows位图，位图大小为640x360，颜色数为24。




