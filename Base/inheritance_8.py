# -*- coding: utf-8 -*-

class Animal(object):
	def run(self):
		print('Animals is run...')

# 类的继承
class Dog(Animal):
	def run(self):
		print('Dog is run...')
	pass
class Cat(Animal):
	pass

class Tortoise(Animal):
	def run(self):
		print('Tortoise is run slowly ...')

dog = Dog()
dog.run()

# isinstance() 判读一个变量是否 是某个类型
a = list()
res = isinstance(a,list)
print(res)
result = isinstance(dog,Animal)
print(result)

def run_twice(Animal):
	Animal.run()
	Animal.run()
# run_twice(Animal())
run_twice(Dog())
# 任何依赖Animal作为参数的函数或者方法都可以不加修改地正常运行，原因就在于多态。
# 传入的任意类型，只要是Animal类或者子类，就会自动调用实际类型的run()方法
run_twice(Tortoise())

# “开闭”原则：
# 对扩展开放：允许新增Animal子类；
# 对修改封闭：不需要修改依赖Animal类型的run_twice()等函数。

class Timer(object):
	def run(self):
		print("Start ..")

timer = Timer()
timer.run()

# Python的“file-like object“就是一种鸭子类型。对真正的文件对象，它有一个read()方法，返回其内容。但是，许多对象，只要有read()方法，都被视为“file-like object“。许多函数接收的参数就是“file-like object“，你不一定要传入真正的文件对象，完全可以传入任何实现了read()方法的对象。




