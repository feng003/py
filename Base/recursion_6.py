# -*- coding: utf-8 -*-
# 递归调用的次数过多，会导致栈溢出。可以试试fact(1000)  Python标准的解释器没有针对尾递归做优化，任何递归函数都存在栈溢出的问题。
def fact(n):
	if n ==1:
		return 1
	return n*fact(n-1)
# print(fact(1000))

def fact(n):
	return fact_iter(n,1)
def fact_iter(num,pro):
	if num == 1:
		return pro
	return fact_iter(num-1,num*pro)

print(fact(10))
# 利用递归函数移动汉诺塔:
def move(n,a,b,c):
	if n == 1:
		print('move',a,'-->',c)
		return
	move(n-1,a,c,b)
	print('move',a,'-->',c)
	move(n-1,b,a,c)
move(3,'a','b','c')