# -*- coding: utf-8 -*-
# 定制类

class Student(object):
	"""docstring for Student"""
	def __init__(self, name):
		self.name = name
	def __str__(self):
		return 'Student object (name: %s)' %self.name
	# __repr__ = __str__

print(Student('John'))
s= Student('Mark')
s

class Fib(object):
	def __init__(self):
		self.a,self.b = 0,1
	# __iter__ 该方法返回一个迭代对象
	def __iter__(self):
		return self
	# 调用该迭代对象的__next__()方法拿到循环的下一个值
	def __next__(self):
		self.a,self.b = self.b, self.a+self.b
		if self.a>100:
			raise StopIteration();   #遇到StopIteration错误时退出循环。
		return self.a
	def __getitem__(self,n):
		if isinstance(n,int): #n是索引
			a,b =1,1
			for x in range(n):
				a,b = b, a+b
			return a
		if isinstance(n,slice): #n是切片
			start = n.start
			stop = n.stop
			if start is None:
				start = 0
			a,b = 1 ,1
			L = []
			for x in range(stop):
				if x>=start:
					L.append(a)
				a,b = b, a+b
			return L

# 当调用不存在的属性时，比如score，Python解释器会试图调用__getattr__(self, 'score')来尝试获得属性	
	def __getattr__(self,attr):
		if attr == 'score':
			return 100
		raise AttributeError('\'Student\' object has no attribute \'%s\'' % attr)

# for n in Fib():
# 	print(n)

f=Fib()
print(f[2:10])
print(f.score)
print(f.lens)

# 如果把对象看成dict，__getitem__()的参数也可能是一个可以作key的object，例如str。
# 与之对应的是__setitem__()方法，把对象视作list或dict来对集合赋值。
# 最后，还有一个__delitem__()方法，用于删除某个元素。
# __getattr__()方法，动态返回一个属性__getattr__()方法，动态返回一个属性
