# -*- coding: utf-8 -*-
class Student(object):
	__solts__ = ('name','age');  #tuple 定义允许绑定的属性名称，__slots__ 定义的属性仅对当前实例起作用，对继承的子类不起作用

	def get_score(self):
		return self.__score

	def  set_score(self,value):
		if not isinstance(value,int):
			raise ValueError('score must be an interger!')
		if value <0 or value>100:
			raise ValueError('score must between 0-100!')
		self.__score = value

	@property  # 装饰器 负责把一个方法变成属性 调用
	def score(self):
		return self.score

	@score.setter
	def score(self,value):
		if not isinstance(value,int):
			raise ValueError('score must be an interger!')
		if value <0 or value>100:
			raise ValueError('score must between 0-100!')
		self.__score = value
