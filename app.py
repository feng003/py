#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import logging; logging.basicConfig(level=logging.INFO)

import asyncio,os,json,time
from datetime import datetime

from aiohttp import web

def index(request):
	return web.Response(body=b'<h1>Awesome</h1>')

@asyncio.coroutine

def init(loop):
	app = web.Application(loop = loop)
	app.router.add_route('GET','/',index)
	srv = yield from loop.create_server(app.make_handler(),'127.0.0.1',9000)
	logging.info('service started at https://127.0.0.1:9000 ...')
	return srv
loop = asyncio.get_event_loop()
loop.run_until_complete(init(loop))
loop.run_forever()

# 加入middleware、jinja2模板和自注册的支持
app = web.Application(loop=loop,middlewares=[
	logger_factory,response_factory
])
init_jinja2(app,filters = dist(datatime = datatime_filter))
add_route(app,'handlers')
add_static(app)

# 一个middleware可以改变URL的输入、输出，甚至可以决定不继续处理而直接返回。middleware的用处就在于把通用的功能从每个URL处理函数中拿出来，集中放到一个地方。
@asyncio.coroutine
def logger_factory(app,handler):
	@asyncio.coroutine
	def logger(request):
		loggin.info("Resquest:%s %s" % (request.method,request.path))
		return(yield from handler(request))
	return logger



